.. -*- coding: utf-8; indent-tabs-mode:nil; -*-
.. SPDX-FileCopyrightText: 2021 Jayson Dalpé <jayson@exmakhina.com>
.. SPDX-License-Identifier: MIT

################
Pulse controller
################

An Arduino pulse controller controlled by Serial and activated by external pin.


Defines
#######

* Pulsing output: PULSE_PIN (2)
* External triger: TRIGGER_PIN (3)
* Type of activation: TRIGGER_ACTIVATION (FALLING, RISING or CHANGE)
* Trigger skipped: TRIGGER_ON_EACH (1 on each X)
* Debouncing for trigger: DEBOUNCE_MS(200ms)

Serial config
#############

Command can be send to the Arduino Board.

#. ``p:XX\n``  --> Pulsing time(XX) on second
#. ``f:YY\n``  --> Pulsing 1/time(YY) in frequency format

Example:

#. ``f:1000\n`` Output: Set Pulse length 1/time 1000.000Hz Pulse time: 0.0010000s Register value: 49536 and Prescaler: 1
#. ``p:0.01\n`` Output: Set Pulse length 1/time 100.000Hz Pulse time: 0.0100000s Register value: 45536 and Prescaler: 8
#. ``p:4.2e-5\n`` Output: Set Pulse length 1/time 23809.525Hz Pulse time: 0.0000420s Register value: 64864 and Prescaler: 1

Limitations
###########

* Pulse length accuracy: 2-5us
* Compatible boards: AVR

Build
#####

Commande line:

  .. code:: sh
	
	arduino --verbose-build --upload --board arduino:avr:mega:cpu=atmega2560 PulseController/PulseController.ino


License
#######

License: MIT (see code headers).

