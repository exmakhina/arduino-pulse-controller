// Pulse Controller
// SPDX-FileCopyrightText: 2021 Jayson Dalpé <jayson@exmakhina.com>
// SPDX-License-Identifier: MIT

#define PULSE_PIN 2
#define TRIGGER_PIN 3 
#define TRIGGER_ACTIVATION FALLING
#define TRIGGER_ON_EACH 2
#define DEBOUNCE_MS 0

String instructions;
float period = 0.001;
float frequency = 1/period;
long preloadTimer;
int triggerCount = 0;
int preventRebound = 0;

void setup() {
  Serial.begin(115200);

  pinMode(PULSE_PIN, OUTPUT);
  pinMode(TRIGGER_PIN, INPUT_PULLUP);

  digitalWrite(PULSE_PIN, LOW);
  
  // Interrupt on Timer 1
  noInterrupts();
  TCCR1A = 0;
  TCCR1B = 1;
  // Init
  preloadTimer = 65536-(16000000/1/frequency);
  TCNT1 = preloadTimer;
  TIMSK1 |= (1 << TOIE1);
  interrupts();  

  attachInterrupt(digitalPinToInterrupt(TRIGGER_PIN), pin_isr, TRIGGER_ACTIVATION);
}

// External ISR
void pin_isr()
{
  if (preventRebound == 1)
    return;
  preventRebound = 1;
  if (triggerCount == 0)
  {
    TCNT1 = preloadTimer;
    digitalWrite(PULSE_PIN, HIGH);
  }
  triggerCount ++;
  if (triggerCount >= TRIGGER_ON_EACH)
  {
    triggerCount = 0;
  }
}

//timer isr
ISR(TIMER1_OVF_vect)
{
  digitalWrite(PULSE_PIN, LOW);
}

void loop() {

  // Rebound logic
  if (preventRebound == 1)
  {
    delay(DEBOUNCE_MS);
    preventRebound = 0;
  }

  // Serial communication
  if (Serial.available())
  {
    instructions = Serial.readString();
    Serial.println(instructions);
    String parseNumber;
    
    // Intepret pulse us
    float value;
    int index = instructions.indexOf(':');
    parseNumber = instructions.substring(index+1); 

    // Commands parsing
    if (instructions.startsWith("p:"))
    {
      period = parseNumber.toFloat();
      frequency = 1/period;
    }
    else if(instructions.startsWith("f:"))
    {
      frequency = parseNumber.toFloat();
      period = 1/frequency;
    }

    // Prescaler calculation
    long validScale = 0;
    long scale1 = 65536-(16000000/1/frequency);
    long scale8 = 65536-(16000000/8/frequency);
    long scale64 = 65536-(16000000/64/frequency);
    long scale256 = 65536-(16000000/256/frequency);
    long scale1024 = 65536-(16000000/1024/frequency);
    if (scale1 >= 1 && scale1 <=65534)
    {
      validScale = 1;
      TCCR1B = 1;
      preloadTimer = scale1;
    }
    else if(scale8 >= 1 && scale8 <= 65534)
    {
      validScale = 8;
      TCCR1B = 2;
      preloadTimer = scale8;
    }
    else if(scale64 >= 1 && scale64 <= 65534)
    {
      validScale = 64;
      TCCR1B = 3;
      preloadTimer = scale64;
    }
    else if(scale256 >= 1 && scale256 <= 65534)
    {
      validScale = 256;
      TCCR1B = 4;
      preloadTimer = scale256;
    }
    else if(scale1024 >= 1 && scale1024 <= 65534)
    {
      TCCR1B = 5;
      validScale = 1024;
      preloadTimer = scale1024;
    }

    // Apply latest value
    if (validScale != 0)
    {
      Serial.print("Set Pulse length 1/time ");
      Serial.print(frequency, 3);
      Serial.print("Hz Pulse time: ");
      Serial.print(period, 7);
      Serial.print("s Register value: ");
      Serial.print(preloadTimer);
      Serial.print(" and Prescaler: ");
      Serial.println(validScale);
      TCNT1 = preloadTimer;
      TCCR1A = 0;
    }
    else
    {
      Serial.println("Incorrect range");
    }
  }
}
